// import  createStore from  'vuex';



// export default createStore({
//     state:{
//         token:"",
//     },
//     getters:{
//     },
//     mutations:{

//         login(state,token) {
//            state.token = token;
//            localStorage.setItem("token" ,token);
//            alert("Login")
//         },
//         logout(state) {
//             state.token = "",
//             localStorage.removeItem("token");
//             alert("Logout")
//         },
//         initializeStore(state) {
//              if(localStorage.getItem("token")) {
//                 state.token = localStorage.getItem("token")
//              }
//         } 

//     },
//     actions:{

//     },
//     modules:{

//     },
// })


import Vue from 'vue'
import Vuex from 'vuex'
import swal from 'sweetalert';
Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        token: "",
    },
    getters: {},
    mutations: {
        login(state, token) {
            state.token = token;
            localStorage.setItem("token", token);
            swal({
                text: "You are successfully logged in'",
                icon: "info",
                buttons: true,
                dangerMode: false
            })
        },
        logout(state) {
            state.token = "",
                localStorage.removeItem("token");
                swal({
                    text: "You are successfully logged Out'",
                    icon: "info",
                    buttons: true,
                    dangerMode: false
                })
        },
        initializeStore(state) {
            if (localStorage.getItem("token")) {
                state.token = localStorage.getItem("token")
            }
        }

    },
    actions: {

    },
    modules: {

    },
});