import Vue from 'vue'
import VueRouter from 'vue-router';
import SignUp from './components/SignUp';
import SignIn from './components/SignIn';
import VehicleType from './components/VehicleType';
import HomePage from './components/HomePage'
import VehicleYearMakeModal from './components/VehicleYearMakeModal'
import DashboardPage from './components/DashboardPage'
Vue.use(VueRouter);

const routes = [
    {
        name: 'home',
        path: '/home',
        component: HomePage,
    },
    {
        name:'dashboard',
        path: '/dashboard',
        component: DashboardPage,
    },
    {
       
        path: '/SignUp',
        component: SignUp,
    },
   
    {
       
        path: '/SignIn',
        component: SignIn,
    },
    {
        name: 'VehicleType',
        path: '/VehicleType',
        component: VehicleType,
    },
    {
        name: 'VehicleYearMakeModal',
        path: '/VehicleYearMakeModal',
        component: VehicleYearMakeModal,
    },

];


const router = new VueRouter({
    mode: 'history',
    routes
  })

export default router;