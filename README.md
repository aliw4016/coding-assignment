# coding-assignment

## Project setup
```
npm install
```

### Run this command to start JSON server
```
json-server --watch db.json 
```

### Compiles and hot-reloads for development
```
npm run serve
```


### Use this dummy api to login authentication 
```
https://reqres.in/api/login
```

### Use below credentials to login
```
Email     : eve.holt@reqres.in
password  : cityslicka
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```



### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
